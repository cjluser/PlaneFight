#ifndef _APP_CONFIG_
#define _APP_CONFIG_

//背景图片速度
static const float BACKGROUND_SPEED  =  2.0f;

//精灵Tag值
enum SPRITE_TAG {
	BACKGROUND_TAG1 = 1,
	BACKGROUND_TAG2
};

//场景常量
enum SCENE_TAG {
	GAME_START = 0,
	GAME_LOADING,
	GAME_MAIN
};

//敌机类型
typedef struct enemyType {
	char name[20];
	float life;
	float speed;
	float attack;
} EnemyType;
//敌机类型参数
static EnemyType enemy1 = {"1", 10, 2, 50};
static EnemyType enemy2 = {"2", 15, 2, 50};
static EnemyType enemy3 = {"3", 20, 2, 100};



#endif