#ifndef _LOADING_SCENE_
#define _LOADING_SCENE_

#include "cocos2d.h"
USING_NS_CC;

class LoadingScene : public cocos2d::CCScene
{
public:
	LoadingScene(void);
	~LoadingScene(void);

	virtual bool init();

	CREATE_FUNC(LoadingScene);
};

#endif