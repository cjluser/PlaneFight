#ifndef _HERO_
#define _HERO_

#include "cocos2d.h"
USING_NS_CC;

class Hero : public cocos2d::CCNode
{
public:
	Hero(void);
	~Hero(void);

	static Hero* create();

	void loadSprite();
	CCRect getBoundingBox();

	CCSprite* getSprite();
	void loseLife();

	CC_SYNTHESIZE(float, life, Life);
	CC_SYNTHESIZE(float, attack, Attack);
	
private:
	CCSprite *sprite;
};

#endif