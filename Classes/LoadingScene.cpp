#include "LoadingScene.h"
#include "LoadingLayer.h"

LoadingScene::LoadingScene(){
	CCLog("enter LoadingScene!");
}

LoadingScene::~LoadingScene(){

}

bool LoadingScene::init(){
	bool bRet = false;
	do {
		CC_BREAK_IF(!CCScene::init());

		LoadingLayer *layer = LoadingLayer::create();
		this->addChild(layer);
		CCLog("LoadingScene init finished!");
		bRet = true;
	}while(0);
	return bRet;
}