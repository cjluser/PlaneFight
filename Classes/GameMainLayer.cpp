#include "GameMainLayer.h"
#include "EnemyLayer.h"
#include "AppConfig.h"

GameMainLayer::GameMainLayer(){
	offset = CCPoint::CCPoint();
}

GameMainLayer::~GameMainLayer(){

}

bool GameMainLayer::init(){
	bool bRet = false;
	do{
		CC_BREAK_IF(!CCLayer::init());

		CCSize winSize =  CCDirector::sharedDirector()->getWinSize();
		//背景
		CCSprite *bg1 = CCSprite::create("img_bg_level_1.jpg");
		CCSprite *bg2 = CCSprite::create("img_bg_level_1.jpg");
		bg1->setAnchorPoint(CCPointZero);
		bg2->setAnchorPoint(CCPointZero);

		bg1->setPosition(CCPointZero);
		bg2->setPosition(ccp(0, bg1->getContentSize().height));

		this->addChild(bg1, 0, BACKGROUND_TAG1);
		this->addChild(bg2, 0, BACKGROUND_TAG2);

		EnemyLayer *enemyLayer = EnemyLayer::create();
		this->addChild(enemyLayer);

		heroLayer = HeroLayer::create();
		this->addChild(heroLayer);

		//设置可触摸
		this->setTouchEnabled(true);

		this->scheduleUpdate();

		bRet = true;
	}while(0);

	return bRet;
}

void GameMainLayer::update(float dt){
	CCNode *bg1 = this->getChildByTag(BACKGROUND_TAG1);
	CCNode *bg2 = this->getChildByTag(BACKGROUND_TAG2);

	bg1->setPositionY(bg1->getPositionY() - BACKGROUND_SPEED);
	bg2->setPositionY(bg2->getPositionY() - BACKGROUND_SPEED);

	if(bg1->getPositionY() <= -bg1->getContentSize().height){
		bg1->setPositionY(bg1->getContentSize().height);
	}
	if(bg2->getPositionY() <= -bg2->getContentSize().height){
		bg2->setPositionY(bg2->getContentSize().height);
	}
}

bool GameMainLayer::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint touchPoint = pTouch->getLocation();
	CCPoint pos = heroLayer->hero->getSprite()->getPosition();
	offset.x = touchPoint.x - pos.x;
	offset.y = touchPoint.y - pos.y;
	return true;
}

void GameMainLayer::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
	CCSize visibleSize =  CCDirector::sharedDirector()->getWinSize();

	CCPoint touchPoint = pTouch->getLocation();

	//飞机的实际坐标
	float px = touchPoint.x - offset.x;
	float py = touchPoint.y - offset.y;

	CCSprite *plane = heroLayer->hero->getSprite();
	//检测飞机是否越界
	if(px - plane->getContentSize().width/2 < 0.0)
	{
		px = 0.0 + plane->getContentSize().width/2;
	}
	if(px + plane->getContentSize().width/2 > visibleSize.width)
	{
		px = visibleSize.width - plane->getContentSize().width/2;
	}
	if(py - plane->getContentSize().height/2 < 0.0)
	{
		py = 0.0 + plane->getContentSize().height/2;
	}
	if(py + plane->getContentSize().height/2 > visibleSize.height)
	{
		py = visibleSize.height - plane->getContentSize().height/2;
	}

	plane->setPosition(ccp(px, py));
}

void GameMainLayer::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
	
}

void GameMainLayer::registerWithTouchDispatcher()
{
	CCDirector *pDirector=CCDirector::sharedDirector();
	pDirector->getTouchDispatcher()->addTargetedDelegate(this,0,true);
}