#ifndef  _GAME_START_SCENE_
#define  _GAME_START_SCENE_

#include "cocos2d.h"
#include "GameStartLayer.h"

class GameStartScene : public cocos2d::CCScene
{
public:
	GameStartScene(void);
	~GameStartScene(void);

	virtual bool init();
	CREATE_FUNC(GameStartScene);

public:
	GameStartLayer *gameStartLayer;
};
#endif