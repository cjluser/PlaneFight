#include "Enemy.h"
#include "AppConfig.h"

Enemy::Enemy(){
	sprite = NULL;

	life = 0;
	speed = 0;
	attack = 0;
}

Enemy::~Enemy(){

}

Enemy* Enemy::create(int lv){
	Enemy *enemy = new Enemy();
	enemy->autorelease();

	EnemyType type;

	switch(lv){
	case 1:
		type = enemy1;
		break;
	case 2:
		type = enemy2;
		break;
	case 3:
		type = enemy3;
		break;
	}

	enemy->loadSprite(type.name);
	enemy->setLife(type.life);
	enemy->setSpeed(type.speed);
	enemy->setAttack(type.attack);
	
	return enemy;
}

void Enemy::loadSprite(char *name) {
	char str[100] = {0};
	sprintf(str, "enemy%s.png", name);
	sprite = CCSprite::createWithSpriteFrameName(str);
	sprite->setScale(0.75f);
	this->addChild(sprite);
}

CCRect Enemy::getBoundingBox(){
	CCRect rect = sprite->boundingBox();
	CCPoint pos = this->convertToWorldSpace(rect.origin);
	return CCRect(pos.x,pos.y,rect.size.width,rect.size.height);
}

void Enemy::loseLife(){
	life--;
}

CCSprite* Enemy::getSprite(){
	return sprite;
}