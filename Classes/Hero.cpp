#include "Hero.h"

Hero::Hero(){
	sprite = NULL;

	life = 0;
	attack = 0;
}

Hero::~Hero(){

}

Hero* Hero::create(){
	Hero *hero = new Hero;
	hero->autorelease();

	hero->loadSprite();
	return hero;
}

void Hero::loadSprite() {
	
	sprite = CCSprite::createWithSpriteFrameName("1.png");
	this->addChild(sprite);

	//�ɻ�����
	CCArray *animationArray = CCArray::createWithCapacity(11);
	char str[100] = {0};
	for(int i=1;i<12;i++)
	{
		sprintf(str, "%d.png", i);
		CCSpriteFrame *frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
		animationArray->addObject(frame);
	}
	CCAnimation *animation = CCAnimation::createWithSpriteFrames(animationArray, 0.1f);
	animation->setLoops(-1);
	CCAnimate *animate = CCAnimate::create(animation);
	animate->setTag(1);
	sprite->runAction(animate);

}

CCRect Hero::getBoundingBox(){
	sprite->getActionByTag(1);
	CCRect rect = sprite->boundingBox();
	CCPoint pos = this->convertToWorldSpace(rect.origin);
	return CCRect(pos.x,pos.y,rect.size.width,rect.size.height);
}

void Hero::loseLife(){
	life--;
}

CCSprite* Hero::getSprite(){
	return sprite;
}