#ifndef _LOADING_LAYER_
#define _LOADING_LAYER_

#include "cocos2d.h"
USING_NS_CC;

class LoadingLayer : public cocos2d::CCLayer
{
public:
	LoadingLayer(void);
	~LoadingLayer(void);

	virtual bool init();

	CREATE_FUNC(LoadingLayer);
private:
	void nextScene();
private:
	CCSize winSize;
};

#endif