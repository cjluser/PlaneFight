#ifndef _GAME_START_LAYER_
#define _GAME_START_LAYER_

#include "cocos2d.h"
USING_NS_CC;

class GameStartLayer : public cocos2d::CCLayer
{
public:
	GameStartLayer(void);
	~GameStartLayer(void);

	virtual bool init();

	CREATE_FUNC(GameStartLayer);

	void showLogo(float dt);
	void showBtn(float dt);

	void nextScene(CCObject* pSender);
private :
	CCSize winSize;
	CCSprite *bg;
	CCMenuItemImage *startGameItem;
};
#endif