#ifndef _CONTROLLER_
#define _CONTROLLER_

#include "cocos2d.h"
#include "AppConfig.h"

USING_NS_CC;

class Controller : public cocos2d::CCNode
{
public:
	static void nextScene(int idx);
};

#endif