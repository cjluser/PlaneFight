#ifndef _HERO_LAYER_
#define _HERO_LAYER_

#include "cocos2d.h"
#include "Hero.h"
USING_NS_CC;

class HeroLayer : public cocos2d::CCLayer
{
public:
	Hero *hero;
public:
	HeroLayer(void);
	~HeroLayer(void);

	virtual bool init();

	CREATE_FUNC(HeroLayer);
};

#endif