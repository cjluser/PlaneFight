#include "EnemyLayer.h"
#include "Enemy.h"

EnemyLayer::EnemyLayer(){
	allEnemy = CCArray::create();
	allEnemy->retain();

	enemySpriteFrame = NULL;
}

EnemyLayer::~EnemyLayer(){
	allEnemy->release();
	allEnemy = NULL;
}

bool EnemyLayer::init(){
	bool bRet = false;
	do {
		CC_BREAK_IF(!CCLayer::init());
		this->schedule(schedule_selector(EnemyLayer::addEnemy), 2.0f, -1, 3.0f);
		//this->schedule(schedule_selector(EnemyLayer::addEnemy), 3.0f, -1, 5.0f);

		bRet = true;
	}while(0);
	return bRet;
}

void EnemyLayer::addEnemy(float dt){
	Enemy *enemy = Enemy::create(1);
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSize enemySize = enemy->getSprite()->getContentSize();
	
	float minX = enemySize.width / 2;
	float maxX = winSize.width - enemySize.width / 2;
	int rangeX = maxX - minX;
	float actualX=(rand() % rangeX)+minX;

	enemy->setPosition(ccp(actualX, winSize.height + enemySize.height / 2));

	this->allEnemy->addObject(enemy);
	this->addChild(enemy);

	float duration = winSize.height / enemy->getSpeed() / 60.0f;
	CCFiniteTimeAction* actionMove=CCMoveTo::create(duration,ccp(actualX, 0 - enemy->getSprite()->getContentSize().height / 2));
	CCFiniteTimeAction* actionDone=CCCallFuncN::create(this,callfuncN_selector(EnemyLayer::enemyMoveFinished));
	CCSequence* sequence=CCSequence::create(actionMove,actionDone,NULL);
	enemy->runAction(sequence);
}

void EnemyLayer::enemyMoveFinished(CCNode* pSender){
	Enemy* enmey=(Enemy*)pSender;
	this->removeChild(enmey,true);
	this->allEnemy->removeObject(enmey);
}