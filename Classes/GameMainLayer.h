#ifndef _GAME_MAIN_LAYER_
#define _GAME_MAIN_LAYER_

#include "cocos2d.h"
#include "HeroLayer.h"
USING_NS_CC;

class GameMainLayer : public cocos2d::CCLayer
{
public:
	GameMainLayer(void);
	~GameMainLayer(void);

	virtual bool init();
	virtual void update(float dt);
	CREATE_FUNC(GameMainLayer);

	bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);

	void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);

	void registerWithTouchDispatcher();
private:
	CCPoint offset;
	HeroLayer *heroLayer;
};
#endif