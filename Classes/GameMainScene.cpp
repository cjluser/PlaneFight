#include "GameMainScene.h"
#include "GameMainLayer.h"

GameMainScene::GameMainScene(){
	CCLog("enter GameMainScene!");
}

GameMainScene::~GameMainScene(){

}

bool GameMainScene::init(){
	bool bRet = false;
	do 
	{
		CC_BREAK_IF(!CCScene::init());
		
		winSize = CCDirector::sharedDirector()->getVisibleSize();

		GameMainLayer *layer = GameMainLayer::create();
		this->addChild(layer);

		bRet = true;
	}while(0);

	return bRet;
}