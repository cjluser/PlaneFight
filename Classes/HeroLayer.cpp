#include "HeroLayer.h"

HeroLayer::HeroLayer(){
	hero = NULL;
}

HeroLayer::~HeroLayer(){

}

bool HeroLayer::init(){
	bool bRet = false;
	do {
		CC_BREAK_IF(!CCLayer::init());

		hero = Hero::create();
		this->addChild(hero);


		bRet = true;
	}while(0);

	return bRet;
}