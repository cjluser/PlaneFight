#ifndef _ENEMY_
#define _ENEMY_

#include "cocos2d.h"
USING_NS_CC;

class Enemy : public cocos2d::CCNode
{
public:
	Enemy(void);
	~Enemy(void);

	static Enemy* create(int lv);

	void loadSprite(char *name);
	CCRect getBoundingBox();

	CCSprite* getSprite();
	void loseLife();

	CC_SYNTHESIZE(float, life, Life);
	CC_SYNTHESIZE(float, speed, Speed);
	CC_SYNTHESIZE(float, attack, Attack);
	
private:
	CCSprite *sprite;
};

#endif