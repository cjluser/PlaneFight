#include "GameStartLayer.h"
#include "Controller.h"

GameStartLayer::GameStartLayer(void){

}

GameStartLayer:: ~GameStartLayer(void){

}

bool GameStartLayer::init(){
	bool bRet=false;
	do 
	{
		CC_BREAK_IF(!CCLayer::init());
		
		winSize = CCDirector::sharedDirector()->getVisibleSize();

		//����
		bg = CCSprite::create("img_bg_logo.jpg");
		bg->setAnchorPoint(CCPointZero);
		bg->setPosition(CCPointZero);
		this->addChild(bg);
		//��Ļ�·��ɻ�����ͼƬ
		CCSprite *ui_1 = CCSprite::create("ui_start_1.png");
		ui_1->setScaleX(0.8f);
		ui_1->setAnchorPoint(CCPointZero);
		ui_1->setPosition(CCPointZero);
		this->addChild(ui_1);
		//��Ļ���ɻ�����ͼƬ
		CCSprite *ui_2 = CCSprite::create("ui_start_2.png");
		ui_2->setScaleX(0.8f);
		ui_2->setAnchorPoint(CCPointZero);
		ui_2->setPosition(ccp(0.0f, winSize.height * 1 / 2));
		this->addChild(ui_2);
		//��Ļ�Ҳ�ɻ�����ͼƬ
		CCSprite *ui_3 = CCSprite::create("ui_start_2.png");
		ui_3->setScaleX(0.8f);
		ui_3->setRotationY(180);
		ui_3->setAnchorPoint(ccp(0,0));
		ui_3->setPosition(ccp(winSize.width, winSize.height * 1 / 2));
		this->addChild(ui_3);

		//��ϷLOGO
		CCSprite *uiLogo = CCSprite::create("LOGO.png");
		uiLogo->setScale(0);
		uiLogo->setPosition(ccp(winSize.width/2, winSize.height * 3/4));
		this->addChild(uiLogo, 1, 1);

		//��ʼ��Ϸ��ť
		CCSprite *startBtn = CCSprite::create("ui_btn.png");

		//��ʼ��Ϸ����ͼƬ
		CCSprite *startGameText = CCSprite::create("ui_start.png");
		startGameText->setPosition(ccp(startBtn->getContentSize().width/2, startBtn->getContentSize().height * 5 / 8));

		startBtn->addChild(startGameText);

		//��ʼ��Ϸ�˵�
		startGameItem = CCMenuItemImage::create();
		startGameItem->initWithNormalSprite(startBtn, startBtn, NULL, this, menu_selector(GameStartLayer::nextScene));
		startGameItem->setScale(0.0f);
		startGameItem->setPosition(ccp(winSize.width * 1/2, winSize.height * 1/6));
		CCMenu *menu = CCMenu::create(startGameItem, NULL);
		menu->setPosition(CCPointZero);
		this->addChild(menu, 1, 2);

		//�����˶�ʱ��
		float dt = (bg->getContentSize().height - winSize.height) / 2 / 60;
		
		//�����ƶ�
		CCActionInterval *bgMove = CCMoveTo::create(dt, ccp(0, -(bg->getContentSize().height - winSize.height)));
		bg->runAction(bgMove);

		//��ʾ��ʼ��Ϸ��ť
		this->scheduleOnce(schedule_selector(GameStartLayer::showBtn), dt * 2/3);
		//��ʾLOGOͼƬ
		this->scheduleOnce(schedule_selector(GameStartLayer::showLogo), dt);

		bRet=true;
	} while (0);

	return bRet;
}

void GameStartLayer::showLogo(float dt){
	CCNode *uiLogo = this->getChildByTag(1);
	CCActionInterval *zoom = CCScaleTo::create(0.1f, 1.0f);
	CCActionInterval *narrow = CCScaleTo::create(0.1f, 0.4f);
	uiLogo->runAction(CCSequence::createWithTwoActions(zoom, narrow));
}

void GameStartLayer::showBtn(float dt){
	CCActionInterval *zoom = CCScaleTo::create(0.15f, 0.75f);
	CCActionInterval *narrow = CCScaleTo::create(0.1f, 0.5f);
	startGameItem->runAction(CCSequence::createWithTwoActions(zoom, narrow));
}

void GameStartLayer::nextScene(CCObject* pSender){
	Controller::nextScene(GAME_LOADING);
}