#include "LoadingLayer.h"

#include "Controller.h"

LoadingLayer::LoadingLayer(){
	CCLog("enter LoadingLayer!");
}

LoadingLayer::~LoadingLayer(){

}

bool LoadingLayer::init(){
	bool bRet = false;

	do {
		CC_BREAK_IF(!CCLayer::init());

		winSize = CCDirector::sharedDirector()->getWinSize();

		//添加背景
		CCSprite *bg = CCSprite::create("ui_loading_Background.jpg");
		bg->setAnchorPoint(CCPointZero);
		bg->setPosition(CCPointZero);
		this->addChild(bg);
		//进度面板
		CCSprite *process = CCSprite::create("ui_process.png");
		process->setScale(0.75f);
		process->setPosition(ccp(winSize.width/2, winSize.height/2));
		this->addChild(process);
		//指针
		CCSprite *point = CCSprite::create("ui_point.png");
		point->setScale(0.75f);
		point->setAnchorPoint(ccp(0.5, 0.25));
		point->setRotation(-90);
		point->setPosition(ccp(winSize.width/2, winSize.height/2 + winSize.height/2 / 20));
		this->addChild(point);
		
		//进度到一半
		CCActionInterval *action = CCRotateTo::create(1.0f, 0.0f);
		point->runAction(action);

		//进度条
		CCSprite *line = CCSprite::create("loading_line.png");
		CCProgressTo *progress1 = CCProgressTo::create(1.0f, 50.0f);
		CCProgressTimer *lineShow = CCProgressTimer::create(line);
		this->addChild(lineShow);
		lineShow->setScale(0.75);
		lineShow->setAnchorPoint(ccp(0.5f, 0.0f));
		lineShow->setPosition(ccp(winSize.width/2, winSize.height * 0.54));
		lineShow->setType(kCCProgressTimerTypeBar);
		lineShow->setMidpoint(ccp(0.0f, 1.0f));
		lineShow->setBarChangeRate(ccp(1.0f, 0.0f));
		lineShow->runAction(progress1);

		//加载主游戏界面图片，eg:敌机，英雄飞机，子弹，背景，云朵，星星等
		CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("img_plane_enemy.plist", "img_plane_enemy.png");
		CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("plane.plist", "plane.png");

		//进度完成，转换到游戏场景
		CCActionInterval *actionDone = CCRotateTo::create(1.0f, 90.0f);
		point->runAction(CCSequence::create(actionDone, CCCallFunc::create(this, callfunc_selector(LoadingLayer::nextScene)), NULL));

		CCProgressTo *progress2 = CCProgressTo::create(1.0f, 100.0f);
		lineShow->runAction(progress2);

		bRet = true;
	}while(0);

	return bRet;
}

void LoadingLayer::nextScene(){
	Controller::nextScene(GAME_MAIN);
}