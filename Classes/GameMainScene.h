#ifndef _GAME_MAIN_
#define _GAME_MAIN_

#include "cocos2d.h"
USING_NS_CC;

class GameMainScene : public cocos2d::CCScene
{
public :
	GameMainScene(void);
	~GameMainScene(void);

	CREATE_FUNC(GameMainScene);

	virtual bool init();

private:
	CCSize winSize;
};

#endif