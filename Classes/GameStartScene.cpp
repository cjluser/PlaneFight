#include "GameStartScene.h"

USING_NS_CC;

GameStartScene::GameStartScene(void)
{
	gameStartLayer = NULL;
}

GameStartScene::~GameStartScene(void)
{
	
}

bool GameStartScene::init()
{
	bool bRet=false;
	do 
	{
		CC_BREAK_IF(!CCScene::init());
		gameStartLayer=GameStartLayer::create();
		CC_BREAK_IF(!gameStartLayer);
		this->addChild(gameStartLayer);
		bRet=true;
	} while (0);

	return bRet;
}