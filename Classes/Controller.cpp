#include "Controller.h"

#include "GameStartScene.h"
#include "LoadingScene.h"
#include "GameMainScene.h"

void Controller::nextScene(int idx){
	CCScene *pScene = NULL;

	switch(idx) {
	case GAME_START:
		pScene = GameStartScene::create();
		break;
	case GAME_LOADING:
		pScene = LoadingScene::create();
		break;
	case GAME_MAIN:
		pScene = GameMainScene::create();
		break;
	default:
		break;
	}

	CCDirector::sharedDirector()->replaceScene(pScene);
}