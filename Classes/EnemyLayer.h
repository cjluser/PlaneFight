#ifndef _ENEMY_LAYER_
#define _ENEMY_LAYER_

#include "cocos2d.h"
USING_NS_CC;

class EnemyLayer : public cocos2d::CCLayer
{
public:
	EnemyLayer(void);
	~EnemyLayer(void);

	virtual bool init();
	CREATE_FUNC(EnemyLayer);

	void addEnemy(float dt);
	void enemyMoveFinished(CCNode* pSender);

private:
	CCArray *allEnemy;

	CCSpriteFrame *enemySpriteFrame;
};

#endif